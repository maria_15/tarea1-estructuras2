#Universidad de Costa Rica
#Estructuras de computadoras digitales II - IE0521
#María José Arce Marín B60561
#maria.arcemarin@ucr.acr.cr

#librerias utilizadas
#import numpy as np
from predictorBinomial import tamanoUPC
from predictorBinomial import contador
from predictorBinomial import vectam
from predictorBinomial import actualizacionbth
from predictorGlobal import xor
from predictorBinomial import imprimir
import sys

#Metodo actualizacionpht

#actualiza la tabla pht
#recibe valorLetra,uPC, tabla3 y el tamano del registro ph
#retorna nada

def actualizacionpht(valorLetra,uPC,tabla3,ph):
    registro = tabla3[uPC]
    binario = bin(registro)
    string = str(binario)
    tamano = len(string)
    if tamano < ph:
        if valorLetra == 'N\n':
            if registro == 0:
                tabla3[uPC] = 0
            elif registro == 1:
                tabla3[uPC] = 0
            elif registro == 2:
                tabla3[uPC] = 1
            elif registro == 3:
                tabla3[uPC] = 2
        elif valorLetra == 'T\n':
            if registro == 0:
                tabla3[uPC] = 1
            elif registro == 1:
                tabla3[uPC] = 2
            elif registro == 2:
                tabla3[uPC] = 3
            elif registro == 3:
                tabla3[uPC] = 3

#Metodo predictorHistoriaPrivada

#es el metodo destinado a predecir con un predictor de historia privada
#recibe como parametros s,bp, gh ph (esto debido a que se necesita para imprimir la informacion)
#retorna el porcentaje de de prediccion correctas

def predictorHistoriaPrivada(s,bp,gh,ph):
    #bth
    entradasTabla = 2**s
    tabla = [0]*entradasTabla #inicializamos tabla con el tamano de entradas que el usuario desee ademas se inicializa en el estado strong not taken
    tabla3 = [0]*entradasTabla #inicializamos tabla con el tamano de entradas que el usuario desee ademas se inicializa en el estado strong not taken
    #inicializamos contadores
    prediccionCorrectaT = 0
    prediccionCorrectaN = 0
    prediccionIncorrectaT = 0
    prediccionIncorrectaN = 0
    #rango del archivo
    rango = vectam()
    Historia = 0#iniciliazamos en 0
    for r in range(rango):#recorremos el archivo
        linea = sys.stdin.readline() #leemos linea por linea
        if linea == "":
            break#si no tiene nada entonces termina el programa
        else:
            recorte = linea.split(" ") #recortamos c/linea al ver un espacio
            #entonces la posicion 0 sera pc y la posicion 1 letra
            valorPC = recorte[0]
            valorLetra = recorte[1]
            uPC = tamanoUPC(valorPC)
            direccion = xor(valorPC,Historia)
            prediccion = contador(direccion,tabla)
            Historia = tabla3[uPC]
            #verificamos si la prediccion es igual a la letra en la cola
            if prediccion == 'N\n':
            
                if prediccion == valorLetra:
                    
                    prediccionCorrectaN = prediccionCorrectaN + 1 #aumentamos el contados
                else:
                    prediccionIncorrectaN = prediccionIncorrectaN +1#aumentamos el contador
                    

            elif prediccion == 'T\n':
                
                if prediccion == valorLetra:
                    
                    prediccionCorrectaT = prediccionCorrectaT + 1#aumentamos el contador
                    
                else:
                    
                    prediccionIncorrectaT = prediccionIncorrectaT + 1#aumentamos el contador

            actualizacionpht(valorLetra,uPC,tabla3,ph)#actualizamos la gth  ingresando el valor de la letra, la direccion upc,la tabla2 y el tamano de los registros privados ph        
            actualizacionbth(direccion,valorLetra,tabla)#actualizamos la bth ingresando la direccion optenida de la xor y la letra actual y la tabla que al puro inicio es inicializada en 0 
    mul = (prediccionCorrectaT + prediccionCorrectaN)*100
    porcentaje = mul/rango#encontramos el % de predicciones correctas
    imprimir(entradasTabla,rango,prediccionCorrectaT,prediccionCorrectaN ,prediccionIncorrectaT,prediccionIncorrectaN,bp,gh,ph,porcentaje)
    return porcentaje#retornamos porcentaje