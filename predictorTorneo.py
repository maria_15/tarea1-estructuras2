#Universidad de Costa Rica
#Estructuras de computadoras digitales II - IE0521
#María José Arce Marín B60561
#maria.arcemarin@ucr.acr.cr

#librerias utilizadas
#import numpy as np
from predictorGlobal import xor
from predictorBinomial import tamanoUPC
from predictorBinomial import actualizacionbth
from predictorGlobal import actualizacionght
from predictorPrivado import actualizacionpht
from predictorBinomial import contador
from predictorBinomial import vectam
from predictorBinomial import imprimir
from predictorGlobal import contadorGP
import sys

#Metodo predictorTorneo

#es el metodo destinado a predecir con un predictor de torneo
#recibe como parametros s,bp, gh ph (esto debido a que se necesita para imprimir la informacion)
#retorna el porcentaje de de prediccion correctas
def predictorTorneo(s,bp,gh,ph):
    #bth
    entradasTabla = 2**s
    tabla = [0]*entradasTabla #inicializamos tabla con el tamano de entradas que el usuario desee ademas se inicializa en el estado strong not taken
    tabla2 = [0]*entradasTabla#ght
    tabla3 = [0]*entradasTabla#pht
    #inicializamos contadores
    prediccionCorrectaT = 0
    prediccionCorrectaN = 0
    prediccionIncorrectaT = 0
    prediccionIncorrectaN = 0
    #rango del archivo
    rango = vectam()
    HistoriaG = 0#iniciliazamos en 0
    HistoriaP = 0#inicializamos en 0
    for r in range(rango):#recorremos el archivo
        linea = sys.stdin.readline() #leemos linea por linea
        if linea == "":
            break#si no tiene nada entonces termina el programa
        else:
            recorte = linea.split(" ") #recortamos c/linea al ver un espacio
            #entonces la posicion 0 sera pc y la posicion 1 letra
            valorPC = recorte[0]#guardamos la direccion en valorPC
            valorLetra = recorte[1]#guardamos la salida T o N en valorLetra
            direccionG = xor(valorPC,HistoriaG)
            direccionP = xor(valorPC,HistoriaP)
            uPC = tamanoUPC(valorPC)
            prediccion = contadorGP(uPC,tabla)
            prediccionG = contador(direccionG,tabla2)
            prediccionP = contador(direccionP,tabla3)
            HistoriaG = tabla2[uPC]
            HistoriaP = tabla3[uPC]
            if prediccion == 'G':
                if prediccionG == 'N\n':
                    if prediccionG == valorLetra:
                        prediccionCorrectaN = prediccionCorrectaN + 1#aumentamos el contador
                        actualizacionght(valorLetra,uPC,tabla2,gh)#gth
                    else:
                        prediccionIncorrectaN = prediccionIncorrectaN + 1#aumentamos el contador
                        actualizacionght(valorLetra,uPC,tabla2,gh)#gth
                elif prediccionG == 'T\n':
                    if prediccionG == valorLetra:
                        prediccionCorrectaT = prediccionCorrectaT + 1#aumentamos el contador
                        actualizacionght(valorLetra,uPC,tabla2,gh)#gth
                    else:
                        prediccionIncorrectaT = prediccionIncorrectaT + 1#aumentamos el contador
                        actualizacionght(valorLetra,uPC,tabla2,gh)#gth
            elif prediccion  == 'P':
                if prediccionP == 'N\n':
                    if prediccionP == valorLetra:
                        prediccionCorrectaN = prediccionCorrectaN + 1 #aumentamos el contador
                        actualizacionpht(valorLetra,uPC,tabla3,ph)#pht
                
                    else:
                        prediccionIncorrectaN = prediccionIncorrectaN +1 #aumentamos el contador
                        actualizacionpht(valorLetra,uPC,tabla3,ph)#pht
                
                elif prediccionP == 'T\n':
                    if prediccionP == valorLetra:
                        prediccionCorrectaT = prediccionCorrectaT + 1#aumentamos el contador
                        actualizacionpht(valorLetra,uPC,tabla3,ph)#pht
                
                    else:
                        prediccionIncorrectaT = prediccionIncorrectaT + 1#aumentamos el contador
                        actualizacionpht(valorLetra,uPC,tabla3,ph)#pht
                
            actualizacionbth(uPC,valorLetra,tabla)#bth
    mul = (prediccionCorrectaT + prediccionCorrectaN)*100
    porcentaje = mul/rango#encontramos % de predicciones correctas
    imprimir(entradasTabla,rango,prediccionCorrectaT,prediccionCorrectaN ,prediccionIncorrectaT,prediccionIncorrectaN,bp,gh,ph,porcentaje)
    return porcentaje#retornamos porcentaje



            


