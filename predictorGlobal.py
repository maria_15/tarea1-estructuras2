#Universidad de Costa Rica
#Estructuras de computadoras digitales II - IE0521
#María José Arce Marín B60561
#maria.arcemarin@ucr.acr.cr

#librerias utilizadas
#import numpy as np
from predictorBinomial import tamanoUPC
from predictorBinomial import contador
from predictorBinomial import vectam
from predictorBinomial import actualizacionbth
from predictorBinomial import imprimir
import sys

#Metodo xor

#es el metodo destinado a hacer una xor con la direccion PC y la historia
#le ingresan como parametros PC y la historia
#retorna xoor que es un valor entero de 0-3 

def xor(uPC,historia):
        entero=int(historia)
        entero2 = int(uPC)
        xor = str(entero^entero2)
        xoor = tamanoUPC(xor)
        return xoor

#Metodo contadorGP

#es el destinado a predecir si es G o P 
#recibe como parametros uPC y la tabla
#retorna P o G 

def contadorGP(uPC,tabla):
    contador = tabla[uPC]
    if contador == 0:   
        return 'P'
    elif contador == 1:
        return 'P'
    elif contador == 2:       
        return 'G'
    elif contador == 3:       
        return 'G'

#Metodo actualizacionght

#es el metodo para actualizar la tabla ght
#recibe como parametros el valorLetra, uPc , la tabla y el tamano del registro global
#retorna nada

def actualizacionght(valorLetra,uPC,tabla2,gh):
    registro = tabla2[uPC]
    binario = bin(registro)
    string = str(binario)
    tamano = len(string)
    if tamano < gh:
        if valorLetra == 'N\n':
            if registro == 0:
                tabla2[uPC] = 0
            elif registro == 1:
                tabla2[uPC] = 0
            elif registro == 2:
                tabla2[uPC] = 1
            elif registro == 3:
                tabla2[uPC] = 2
        elif valorLetra == 'T\n':
            if registro == 0:
                tabla2[uPC] = 1
            elif registro == 1:
                tabla2[uPC] = 2
            elif registro == 2:
                tabla2[uPC] = 3
            elif registro == 3:
                tabla2[uPC] = 3



#Metodo predictorHistoriaGlobal

#es el metodo destinado a predecir con un predictor de historia global
#recibe como parametros s,bp, gh ph (esto debido a que se necesita para imprimir la informacion)
#retorna el porcentaje de de prediccion correctas

def predictorHistoriaGlobal(s,bp,gh,ph):
    #bth
    entradasTabla = 2**s
    tabla = [0]*entradasTabla #inicializamos tabla con el tamano de entradas que el usuario desee ademas se inicializa en el estado strong not taken
    tabla2 = [0]*entradasTabla #inicializamos tabla con el tamano de entradas que el usuario desee ademas se inicializa en el estado strong not taken
    #inicializamos contadores
    prediccionCorrectaT = 0
    prediccionCorrectaN = 0
    prediccionIncorrectaT = 0
    prediccionIncorrectaN = 0
    #rango del archivo
    rango = vectam()
    Historia = 0#iniciliazamos en 0
    for r in range(rango):#recorremos el archivo
        linea = sys.stdin.readline() #leemos linea por linea
        if linea == "":
            break#si no tiene nada entonces termina el programa
        else:
            recorte = linea.split(" ") #recortamos c/linea al ver un espacio
            #entonces la posicion 0 sera pc y la posicion 1 letra
            valorPC = recorte[0]
            valorLetra = recorte[1]
            uPC = tamanoUPC(valorPC)
            direccion = xor(valorPC,Historia)
            prediccion = contador(direccion,tabla)
            Historia = tabla2[uPC]
            #verificamos si la prediccion es igual a la letra en la cola
            if prediccion == 'N\n':
            
                if prediccion == valorLetra:
                    
                    prediccionCorrectaN = prediccionCorrectaN + 1 #aumentamos el contados
                else:
                    prediccionIncorrectaN = prediccionIncorrectaN +1#aumentamos el contador
                    

            elif prediccion == 'T\n':
                
                if prediccion == valorLetra:
                    
                    prediccionCorrectaT = prediccionCorrectaT + 1#aumentamos el contador
                    
                else:
                    
                    prediccionIncorrectaT = prediccionIncorrectaT + 1#aumentamos el contador

            actualizacionght(valorLetra,uPC,tabla2,gh)#actualizamos la gth  ingresando el valor de la letra, la direccion upc,la tabla2 y el tamano de los registros globales gh        
            actualizacionbth(direccion,valorLetra,tabla)#actualizamos la bth ingresando la direccion optenida de la xor y la letra actual y la tabla que al puro inicio es inicializada en 0 
    mul = (prediccionCorrectaT + prediccionCorrectaN)*100
    porcentaje = mul/rango#encontramos el % de predicciones correctas
    imprimir(entradasTabla,rango,prediccionCorrectaT,prediccionCorrectaN ,prediccionIncorrectaT,prediccionIncorrectaN,bp,gh,ph,porcentaje)
    return porcentaje#retornamos porcentaje

       




       


        
